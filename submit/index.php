<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

$myfile = fopen("username.txt", "r");
$userName = trim(fread($myfile,filesize("username.txt")));

$json = file_get_contents('php://input');

$jsonObj = json_decode($json);

$firstName = $jsonObj->firstName;
$lastName =  $jsonObj->lastName;
$DOB =  $jsonObj->frontEndDOB;
$patID = $lastName.", ".$firstName." (".$DOB.")";
$patPath = "iTsQ205GGZ9IbVnDW+21p82wKeoo2O+b0vcTOhouZpY/".$patID."/";
mkdir($patPath);


//personal.ini file 
$personalINI = fopen($patPath."personal.ini","w");
fwrite($personalINI,"[Address]\n");
fwrite($personalINI,"City=".$jsonObj->homeCity."\n");
fwrite($personalINI,"Country=".$jsonObj->homeCountry."\n");
fwrite($personalINI,"State=".$jsonObj->homeState."\n");
fwrite($personalINI,"StreetAddr1=".$jsonObj->homeAddr1."\n");
fwrite($personalINI,"StreetAddr2=".$jsonObj->homeAddr2."\n");
fwrite($personalINI,"Zip=".$jsonObj->homeZip."\n");

fwrite($personalINI,"\n[Emails]\n");
fwrite($personalINI,"Email=".$jsonObj->emailAddr."\n");

fwrite($personalINI,"\n[Emergency]\n");
fwrite($personalINI,"EmergencyName=".$jsonObj->emergencyName."\n");
fwrite($personalINI,"EmergencyRelation=".$jsonObj->emergencyRel."\n");
fwrite($personalINI,"EmergencyPhone=".$jsonObj->emergencyPhone."\n");

fwrite($personalINI,"\n[Name]\n");
fwrite($personalINI,"FirstName=".$firstName."\n");
fwrite($personalINI,"LastName=".$lastName."\n");
fwrite($personalINI,"MiddleName=".$jsonObj->middleName."\n");
fwrite($personalINI,"PreferredName=".$jsonObj->preferredName."\n");

fwrite($personalINI,"\n[Personal]\n");
fwrite($personalINI,"DateOfBirth=".$jsonObj->DOB."\n");
fwrite($personalINI,"Ethnicity=".$jsonObj->ethnicity."\n");
fwrite($personalINI,"Gender=".$jsonObj->gender."\n");
fwrite($personalINI,"Race=".$jsonObj->race."\n");
fwrite($personalINI,"Sex=".$jsonObj->sex."\n");

fwrite($personalINI,"\n[Phones]\n");
fwrite($personalINI,"CellPhone=".$jsonObj->cellNumber."\n");
fwrite($personalINI,"HomePhone=".$jsonObj->homePhoneNumber."\n");

fwrite($personalINI,"\n[Preferences]\n");
fwrite($personalINI,"AvailableDays=".$jsonObj->available."\n");
fwrite($personalINI,"PreferredContact=".$jsonObj->bestContact."\n");

fwrite($personalINI,"\n[Work]\n");
fwrite($personalINI,"AvailableDays=".$jsonObj->available."\n");
fwrite($personalINI,"PreferredContact=".$jsonObj->bestContact."\n");

fclose($personalINI);


//Interview file
$newPatINI = fopen($patPath."newPatInterview.ini","w");
fwrite($newPatINI,"[Pain]\n");
fwrite($newPatINI,"IsInPain=".$jsonObj->pain."\n");
fwrite($newPatINI,"PainLength=".$jsonObj->painLength."\n");
fwrite($newPatINI,"PainLocation=".$jsonObj->painLocation."\n");
fwrite($newPatINI,"SaveTooth=".$jsonObj->saveTooth."\n");
fwrite($newPatINI,"WhenCome=".$jsonObj->whenCome."\n");

fwrite($newPatINI,"\n[DentalExp]\n");
fwrite($newPatINI,"PastIssues=".$jsonObj->pastIssues."\n");
fwrite($newPatINI,"LastCleaning=".$jsonObj->lastCleaning."\n");
fwrite($newPatINI,"LastSCRP=".$jsonObj->lastSCRP."\n");
fwrite($newPatINI,"OftenBrush=".$jsonObj->oftenBrush."\n");
fwrite($newPatINI,"OftenFloss=".$jsonObj->oftenFloss."\n");
fwrite($newPatINI,"OftenBleed=".$jsonObj->oftenBleed."\n");
fwrite($newPatINI,"OftenHeadaches=".$jsonObj->oftenHeadaches."\n");
fwrite($newPatINI,"OftenGrinding=".$jsonObj->oftenGrinding."\n");
fwrite($newPatINI,"LookingToGetDone=".$jsonObj->lookingToGetDone."\n");
fwrite($newPatINI,"BracesBefore=".$jsonObj->bracesBefore."\n");

fclose($newPatINI);

//Dental Plan Info 
$dentalPlanINI = fopen($patPath."dentalPlans.ini","w");
fwrite($dentalPlanINI,"[Primary]\n");
fwrite($dentalPlanINI,"InsName=".$jsonObj->insName."\n");
fwrite($dentalPlanINI,"InsRelToPat=".$jsonObj->insRelToPat."\n");
fwrite($dentalPlanINI,"InsSSN=".$jsonObj->insSSN."\n");
fwrite($dentalPlanINI,"InsCompany=".$jsonObj->insCompany."\n");
fwrite($dentalPlanINI,"InsGroup=".$jsonObj->insGroup."\n");
fwrite($dentalPlanINI,"InsSubID=".$jsonObj->insSubID."\n");
fwrite($dentalPlanINI,"InsAddr=".$jsonObj->insAddr."\n");

fwrite($dentalPlanINI,"\n[Secondary]\n");
fwrite($dentalPlanINI,"InsName=".$jsonObj->secInsName."\n");
fwrite($dentalPlanINI,"InsRelToPat=".$jsonObj->secInsRelToPat."\n");
fwrite($dentalPlanINI,"InsSSN=".$jsonObj->secInsSSN."\n");
fwrite($dentalPlanINI,"InsCompany=".$jsonObj->secInsCompany."\n");
fwrite($dentalPlanINI,"InsGroup=".$jsonObj->secInsGroup."\n");
fwrite($dentalPlanINI,"InsSubID=".$jsonObj->secInsSubID."\n");
fwrite($dentalPlanINI,"InsAddr=".$jsonObj->secInsAddr."\n");

fclose($dentalPlanINI);


//now medications.json
$medObj = $jsonObj->medList;
$medListArray = array();

foreach($medObj as $i => $drug) {
    $addMe = array( "DrugName" => $drug->drugName , "UsedFor" => $drug->usedFor, "PrescribedHere"=> false, "Schedule" => $drug->schedule);
    array_push($medListArray,$addMe);
}

$medicationsJSON = fopen($patPath."medications.json","w");
fwrite($medicationsJSON,json_encode($medListArray));
fclose($medicationsJSON);


//now medical conditions
$condObj = $jsonObj->medicalConditions;
$condListArray = array();

foreach($condObj as $conditionName => $conditionInfo) {
    $addMe = array( "ConditionName" => $conditionName , "ConditionInfo" => $conditionInfo);
    array_push($condListArray,$addMe);
}

$conditionsJSON = fopen($patPath."conditions.json","w");
fwrite($conditionsJSON,json_encode($condListArray));
fclose($conditionsJSON);


//MedicalInfo.ini
$medicalInfoINI = fopen($patPath."medicalInfo.ini","w");
fwrite($medicalInfoINI,"[Pregnancy]\n");
fwrite($medicalInfoINI,"IsPregnant=".$jsonObj->pregnant."\n");
fwrite($medicalInfoINI,"DueDate=".$jsonObj->pregnantDueDate."\n");
fwrite($medicalInfoINI,"OBGYN=".$jsonObj->pregnantOBGYN."\n");
fwrite($medicalInfoINI,"IsBreastfeeding=".$jsonObj->breastFeeding."\n");

fwrite($medicalInfoINI,"\n[AntibioticPreMed]\n");
fwrite($medicalInfoINI,"NeedPreMed=".$jsonObj->needPreMed."\n");
fwrite($medicalInfoINI,"WhatTakePremed=".$jsonObj->whatTakePremed."\n");
fwrite($medicalInfoINI,"WhoToldTakePremed=".$jsonObj->whoToldTakePremed."\n");

fwrite($medicalInfoINI,"\n[Cancer]\n");
fwrite($medicalInfoINI,"HxOfCancer=".$jsonObj->hxOfCancer."\n");
fwrite($medicalInfoINI,"CancerType=".$jsonObj->cancerType."\n");
fwrite($medicalInfoINI,"HxChemo=".$jsonObj->hxChemo."\n");
fwrite($medicalInfoINI,"HxRad=".$jsonObj->hxRad."\n");

fclose($medicalInfoINI);


//Now allergies
$allergiesObj = $jsonObj->allergyList;
$allergyListArray = array();

foreach($allergiesObj as $i => $allergy) {
    $addMe = array( "AllergyName" => $allergy->allergyName , "AllergyReaction" => $allergy->reaction);
    array_push($allergyListArray,$addMe);
}

$allergiesJSON = fopen($patPath."allergies.json","w");
fwrite($allergiesJSON,json_encode($allergyListArray));
fclose($allergiesJSON);

//Save the consent dots
mkdir($patPath."images/consents/newPatConsent", 0777, true);
$consentDotFile = fopen($patPath."images/consents/newPatConsent/consentDots.json","w");
fwrite($consentDotFile,json_encode($jsonObj->consentSigDots));
fclose($consentDotFile);

echo "chal";

?>
