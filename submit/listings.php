<?php
//error_reporting(E_ERROR | E_PARSE);
$dir = ".";
$patDirs = array_diff(scandir($dir),array('..', '.','index.php'));
$returnMeObj = array();
foreach($patDirs as $i => $dirName) {
	$personalINI = parse_ini_file($dirName."/personal.ini",true);
	$interviewINI = parse_ini_file($dirName."/newPatInterview.ini",true, INI_SCANNER_RAW);
	$submitTime = filemtime($dirName."/personal.ini");
	$addMe = array( "Personal" => $personalINI, "Interview" => $interviewINI, "URL" =>$dirName , "SubmitTime" => date("F j, Y, g:i a", $submitTime));
    array_push($returnMeObj,$addMe);
}
echo json_encode($returnMeObj);
?>
