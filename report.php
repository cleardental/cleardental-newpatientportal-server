<html>
    <head>
        <title>New Patient Report</title>
        <style>
            table, th, td {
            border: 1px solid black;
            }
        </style>
    </head>
    <body>
    <h1>New Patient Report</h1>
    <table>
    <tr><th>Field Name</th><th>Field Value</th></tr>


<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$myfile = fopen("username.txt", "r");

$userName = trim(fread($myfile,filesize("username.txt")));
fclose($myfile);
$submitDir = "/home/".$userName."/submits/";
$fullFilePath = $submitDir.$_GET["submit"];

$file = fopen($fullFilePath, "r");
$jsonData = fread($file,filesize($fullFilePath));
$nativeData = json_decode($jsonData);
$valueDict = [
    "cc" => "Chief Complaint",
    "firstName" => "First Name",
    "middleName" => "Middle Name",
    "lastName" => "Last Name",
    "preferredName" => "Preferred Name",
    "DOB" => "Date of Birth",
    "sex" => "Sex",
    "gender" => "Gender",
    "race" => "Race",
    "ethnicity" => "Ethnicity",
    "refSource" => "Referral source",
    "refName" => "Referral's name",
    "homeAddr1" => "Home Address (Line 1)",
    "homeAddr2" => "Home Address (Line 2)",
    "homeCity" => "Home Address City",
    "homeState" => "Home Address State",
    "homeZip" => "Home Address Zip",
    "homeCountry" => "Home Country",
    "homePhoneNumber" => "Home Phone Number",
    "cellNumber" => "Cell Phone Number",
    "workNumber" => "Work Number",
    "emailAddr" => "Email Address",
    "whatsAppNumb" => "WhatsApp Number",
    "facebookName" => "Facebook Name / URL",
    "instaName" => "Instagram Name",
    "bestContact" => "Best Way to Contact",
    "available" => "Days Available",
    "hasDentalPlan" => "Has a dental plan?",
    "insName" => "Insured’s Name",
    "insRelToPat" => "Relationship to Patient",
    "insSSN" => "Social Security #",
    "insCompany" => "Insurance Company",
    "insGroup" => "Group Number",
    "insSubID" => "Subscriber ID",
    "insAddr" => "Insurance Company Address",
    "yesAddr" => "Has Additional Coverage",
    "pastIssues" => "Do you have any past issues with the dentist?",
    "lastCleaning" => "When was your last cleaning?",
    "lastSCRP" => "Have you ever had \"scaling and root planing\"<br> or a \"deep cleaning\" done before?",
    "oftenBrush" => "How often do you brush?",
    "oftenFloss" => "How often do you floss?",
    "oftenBleed" => "Does your gum bleed when you brush or floss?",
    "oftenHeadaches" => "Do you have headaches?",
    "oftenGrinding" => "Do you know if you grind at night?",
    "lookingToGetDone" => "What are you looking to get done?",
    "bracesBefore" => "Have you ever had braces before?",
    "anyTroubleSleeping" => "Do you have any trouble sleeping?",
    "emergencyName" => "Emergency Contact name",
    "emergencyRel" => "Emergency Contact Relationship",
    "emergencyPhone" => "Emergency Contact phone number",
    "secInsName" => "Secondary Insured’s Name",
    "secInsRelToPat" => "Secondary Relationship to Patient",
    "secInsSSN" => "Secondary Social Security #",
    "secInsCompany" => "Secondary Insurance Company",
    "secInsGroup" => "Secondary Group Number",
    "secInsSubID" => "Secondary Subscriber ID",
    "secInsAddr" => "Secondary Insurance Company Address"
];
foreach($nativeData as $attr => $val) {
    if($attr == "consentSigDots") {
        echo "<tr><td>Consent signature</td><td> <canvas id=\"myCanvas\" width=\"720\" height=\"450\" style=\"border:1px solid #000000;\"></canvas> </td></tr>";
        echo "<script>var c = document.getElementById(\"myCanvas\");var ctx = c.getContext(\"2d\");ctx.beginPath();";
        for($i =0; $i < count($val); $i+=2) {
            if($val[$i]==",") {
                echo " ctx.stroke();ctx.beginPath();";
            
            }
            else {
                echo "ctx.lineTo(".$val[$i].",".$val[$i+1].");";
            }
            echo " ctx.stroke();";
        }
        echo "</script>";
        
    }
    else if($attr == "medicalConditions") {
        echo "<tr><td>Medical Conditions</td><td>";
        foreach($val as $cond => $extraInfo) {
            if($cond == "cancer") {
                echo "History of Cancer: ";
                echo $val->cancerType."; ";
                if($val->hxChemo == "true") {
                    echo "had Chemotherapy; ";
                }
                if($val->hxRad == "true") {
                    echo "had radiation treatment; ";
                }
                echo "<br>";
            }
            else if(($cond == "cancerType")||($cond == "hxChemo")||($cond == "hxRad")||($cond == "pregnant-duedate")||($cond == "pregnant-OBGYN")) {
                //do nothing!
            }
            else if($cond == "otherCond") {
                echo "Other conditions: ";
                echo str_replace("\n","; ",$val->$cond);
                echo "<br>";
            }
            else if($cond == "breastFeeding") {
                echo "Patient is Breastfeeding<br>";
            }
            else if($cond == "pregnant") {
                if($extraInfo == "true") {
                    echo "Patient is pregnant: ";
                    $duedateString = "pregnant-duedate"; //because PHP is incredibly stupid!
                    $obgynString = "pregnant-OBGYN";
                    echo "Due date is ".$val->$duedateString."; ";
                    echo "Patient's OBGYN is ".$val->$obgynString."<br>";
                } else {
                    echo "Patient is NOT pregnant<br>";
                }
                
            }
            
            else {
                echo $cond;
                if($val->$cond != "true") {
                    echo " (".$val->$cond.")";
                }
                echo "<br>";
            }
            
        }
        echo "</td></tr>";
    }
    else if($attr == "allergyList") {
        echo "<tr><td>Allergies</td><td>";
        
        echo "<table><tr><th>Allergy Name</th><th>Reaction</th></tr>";
        
        foreach($val as $i => $allergy) {
            echo "<tr><td>".$allergy->allergyName."</td><td>".$allergy->reaction."</td></tr>";
        }
        
        echo "</table></td>";
        
    }
    else if($attr == "medList") {
        echo "<tr><td>Medications</td><td>";
        
        echo "<table><tr><th>Drug Name</th><th>Used for What</th><th>Amount</th><th>How Often</th></tr>";
        
        foreach($val as $i => $drug) {
            echo "<tr><td>".$drug->drugName."</td><td>".$drug->usedFor."</td><td>".$drug->amount."</td><td>".$drug->schedule."</td></tr>";
        }
        
        echo "</table></td>";
    }
    else if($attr == "consentSigDotCount") {
        //ignroed for now...
    }
    else if($attr == "pain") {
        if($val) {
            echo "<tr><td>In Pain</td><td>Yes</td></tr>";
        }
        else {
            echo "<tr><td>In Pain</td><td>No</td></tr>";
        }
    }
    else if(array_key_exists($attr,$valueDict)) {
        echo "<tr><td>".$valueDict[$attr]."</td><td>".$val."</td></tr>";
    
    }
    else {
        echo "<tr><td>".$attr."</td><td>".$val."</td></tr>";
    }
    
}
?>
        </table>
    </body>
</html>
