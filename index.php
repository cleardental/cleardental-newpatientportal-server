<html>
    <head>
        <title>Current submissions</title>
        <style>
            table, th, td {
            border: 1px solid black;
            }
        </style>
    </head>
    <body>
    <h1>Current submissions</h1>
    <table>
    <tr><th>First Name</th><th>Last Name</th><th>Date of Birth</th><th>Submission Time</th><th>View Report</th></tr>

<?php
date_default_timezone_set('America/New_York');

$myfile = fopen("username.txt", "r");
$userName = trim(fread($myfile,filesize("username.txt")));
fclose($myfile);
$submitDir = "/home/".$userName."/submits/";
$fakeResults = scandir($submitDir);
$results = array_slice($fakeResults,2);

foreach($results as $key => $filename) {
    //echo $filename."<br>";
    $fullFilePath = $submitDir.$filename;
    $filePointer = fopen($fullFilePath,"r");
    $jsonData = fread($filePointer,filesize($fullFilePath));
    $nativeData = json_decode($jsonData);
    $firstName = "";
    $lastName = "";
    $dob = "";
    foreach($nativeData as $attr => $val) {
        if($attr == "firstName") {
            $firstName = $val;
        } else if($attr == "lastName") {
            $lastName = $val;
        } else if($attr == "DOB") {
            $dob = $val;
        }
    }


    echo "<tr><td>".$firstName."</td><td>".$lastName."</td><td>".$dob."</td><td>".date("m/d/Y g:i:s A", rtrim($filename,".json"))."</td><td><a href=\"report.php?submit=".$filename."\">See report</a></td></tr>";
}
?>
        </table>
    </body>
</html>
